﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcCustomer;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Text.Json;
using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class grpcCustomerService : GrpcCustomer.grpcCustomer.grpcCustomerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public grpcCustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<soResponce> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return new soResponce() { SerializedObj = JsonSerializer.Serialize(response) };
        }

        public override async Task<soResponce> GetCustomer(idRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new CustomerResponse(customer);

            return new soResponce() { SerializedObj = JsonSerializer.Serialize(response)};
        }
        
        public override async Task<soResponce> CreateCustomer(CrCustReq request, ServerCallContext context)
        {
            var prefids = JsonSerializer.Deserialize<List<Guid>>(request.PrefIdsSer);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(prefids);
            var croredcust = new CreateOrEditCustomerRequest()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PreferenceIds = prefids,
            };
            Customer customer = CustomerMapper.MapFromModel(croredcust, preferences);

            await _customerRepository.AddAsync(customer);

            return new soResponce() { SerializedObj = JsonSerializer.Serialize(customer.Id) };
        }
        public override async Task<soResponce> EditCustomer(EditCustReq request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)  return new soResponce() { SerializedObj = JsonSerializer.Serialize(new { }) };
            var prefids = JsonSerializer.Deserialize<List<Guid>>(request.Req.PrefIdsSer);
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(prefids);

            var croredcust = new CreateOrEditCustomerRequest()
            {
                FirstName = request.Req.FirstName,
                LastName = request.Req.LastName,
                Email = request.Req.Email,
                PreferenceIds = prefids,
            };

            customer = CustomerMapper.MapFromModel(croredcust, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new soResponce() { SerializedObj = JsonSerializer.Serialize(customer.Id) };
        }
        public override async Task<bRes> DeleteCustomer(idRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new bRes() { Res = false };

            await _customerRepository.DeleteAsync(customer);
            return new bRes() { Res = true };
        }
    }
}
