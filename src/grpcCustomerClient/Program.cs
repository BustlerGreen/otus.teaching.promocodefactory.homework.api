﻿using Grpc.Net.Client;
using System;
using System.Threading.Tasks;
using GrpcCustomer;
using System.Text.Json;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

using var channel = GrpcChannel.ForAddress("https://localhost:5001");
var client = new grpcCustomer.grpcCustomerClient(channel);
var par = new Google.Protobuf.WellKnownTypes.Empty();
var customers = client.GetCustomers(par);
Console.WriteLine(customers);
var customer = client.GetCustomer(new idRequest() { Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0" });
Console.WriteLine(customer);

var crc = new CrCustReq()
{
    Email = "semen_valtsov@mail.ru",
    FirstName = "Cемен",
    LastName = "вальцов",
    PrefIdsSer = JsonSerializer.Serialize(new List<Guid>() { Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") }),
};
customer = client.CreateCustomer(crc);
Console.WriteLine(customer);

var edc = new EditCustReq()
{
    Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0",
    Req = new CrCustReq()
    {
        Email = "semen_valtsov@mail.ru",
        FirstName = "Cемен",
        LastName = "вальцов",
        PrefIdsSer = JsonSerializer.Serialize(new List<Guid>() { Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") }),
    }
};
customer = client.EditCustomer(edc);
Console.WriteLine(customer);

var res = client.DeleteCustomer(new idRequest() { Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0" });
Console.WriteLine(res);
Console.Read();